var gulp = require('gulp');
var sass = require('gulp-sass');
var ngAnnotate = require('gulp-ng-annotate');
var concat = require('gulp-concat');

var path = {
    js: 'app/js/',
    sass: 'app/sass/'
};

gulp.task('default', ['js', 'sass']);

gulp.task('js', function(){
    gulp.src(path.js + '**/*.js')
        .pipe(concat('main.js'))
        .pipe(ngAnnotate())
        .pipe(gulp.dest('build'));
});

gulp.task('sass', function(){
    gulp.src(path.sass + '**/*.sass')
        .pipe(sass())
        .pipe(gulp.dest('build'))
});

gulp.task('watch', ['js', 'sass'], function(){
    gulp.watch(path.js + '**/*.js', ['js']);
    gulp.watch(path.sass + '**/*.sass', ['sass']);
});
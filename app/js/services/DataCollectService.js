app.service('DataCollector', function ($http, $rootScope) {
    var DataCollector = this;
    var collectedData = [];

    //Get tree data
    (function () {
        if (!collectedData.length) {
            $http.get('data.json')
                .then(function (res) {
                    collectedData = res.data;
                    DataCollector.notify();
                }, function () {
                    //Handling error if couldn't get data.
                    alert('Could not get data.')
                });
        }
    })();


    //Add new node.
    this.setData = function (input, ID) {
        var uniqueID;
        (function () {
            var ids = [];
            var cycle = function (data) {
                for (var i = 0; data.length > i; i++) {
                    ids.push(data[i].ID);
                    if (data[i].Children) {
                        cycle(data[i].Children);
                    }
                }
            };

            cycle(collectedData);
            uniqueID = Math.max.apply(null, ids) + 1;
        })();
        
        var doCycle = function (data) {
            var found = false;
            for (var i = 0; data.length > i; i++) {
                //Checking by ID.
                if (data[i].ID == ID) {
                    if (data[i].Children !== undefined) {
                        data[i].Children.push({
                            "ID": uniqueID,
                            "Title": input
                        });
                    } else {
                        data[i].Children = [{
                            "ID": uniqueID,
                            "Title": input
                        }]
                    }
                    found = true;
                } else {
                    if (data[i].Children !== undefined) {
                        doCycle(data[i].Children);
                    }
                }
            }
            return found;
        };

        //If input is valid with the text, do cycle and try to add new node.
        if (input !== '' && input !== undefined) {
            if (doCycle(collectedData)) this.notify();
        }
    };

    this.returnData = function () {
        return collectedData;
    };

    //Listener of changes
    this.changed = function (scope, callback) {
        var handler = $rootScope.$on('notify', callback);
        scope.$on('$destroy', handler);
    };

    //Triggering the listener
    this.notify = function () {
        $rootScope.$emit('notify');
    };

});

app.controller('SecondController', function ($scope, DataCollector) {
    this.treeData = [];

    //Set new node
    this.setData = function (input, ID, node) {
        DataCollector.setData(input, ID);
        //Cleaning input of new node name on the view.
        node.newName = null;
        //Updating the view
        this.treeData = this.iterateTree(DataCollector.returnData());
    };

    //Watcher for updated data from service.
    (function (_this) {
        DataCollector.changed($scope, function () {
            _this.treeData = _this.iterateTree(DataCollector.returnData());
        });
    })(this);

    //Take array and iterate all content.
    this.iterateTree = function (array) {

        //Cloning array to prevent damaging it on the service.
        var items = JSON.parse(JSON.stringify(array));
        var ns = [];

        //Looping through root elements.
        for (var i = 0; items.length > i; i++) {
            iterate(items[i]);
        }

        //Setting new iterative array of elements.
        function iterate(node) {
            var n = node;
            var parent;
            var depth = 0;
            ns.push({
                ID: n.ID,
                Title: n.Title,
                depth: depth
            });

            //v = visited, cc = children count.
            while (n !== null) {
                parent = n;
                if (n !== undefined) {
                    n.v = true;
                    if (n.Children) {
                        n.cc = n.cc == undefined ? 0 : n.cc + 1;
                        if (n.Children[n.cc] !== undefined) {
                            n = n.Children[n.cc];
                            n.p = parent;
                            depth = depth + 1;
                            if (!n.v) {
                                ns.push({
                                    ID: n.ID,
                                    Title: n.Title,
                                    depth: depth
                                });
                            }
                        } else {
                            depth = depth - 1;
                            n = n.p;
                        }
                    } else {
                        depth = depth - 1;
                        n = n.p;
                    }
                } else {
                    n = null;
                }

            }
        }
        return ns;
    }
});

app.directive('node', function (DataCollector) {
    return {
        scope: {
            category: "=category"
        },
        restrict: 'E',
        replace: true,
        templateUrl: '/app/templates/NodeTemplate.html',
        link: function (scope, e, a) {
            scope.setData = function (input, ID) {
                scope.newName = '';
                DataCollector.setData(input, ID);
                DataCollector.notify();
            }
        }
    }
});
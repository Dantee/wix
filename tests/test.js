'use strict';

describe('MainController', function () {

    beforeEach(module('wix'));

    var $controller;

    beforeEach(inject(function (_$controller_, $rootScope) {
        $scope = $rootScope.$new();
        $controller = _$controller_;
    }));
    var $scope = {};

    it('Tree data should come as function', function () {
        var controller = $controller('MainController', {
            $scope: $scope
        });
        expect(typeof controller.treeData).toEqual('function');
    });

    var demodata = [{
        "Title": "Lorem",
        "Children": [
            {
                "Title": "Ipsum"
            },
            {
                "Title": "Dolor",
                "Children": [
                    {
                        "Title": "Orci",
                        "Children": [
                            {
                                "Title": "Quis",
                                "Children": [
                                    {
                                        "Title": "Odio"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }];

    it('Should return iterative data with depth 0 on the first element', function () {
        var controller = $controller('SecondController', {
            $scope: $scope
        });
        expect(controller.iterateTree(demodata)[0].depth).toEqual(0);
    });
    it('Should return iterative data with depth 1 on the second element', function () {
        var controller = $controller('SecondController', {
            $scope: $scope
        });
        expect(controller.iterateTree(demodata)[1].depth).toEqual(1);
    });
    it('Should return 6 elements length array', function () {
        var controller = $controller('SecondController', {
            $scope: $scope
        });
        expect(controller.iterateTree(demodata).length).toEqual(6);
    });

});


